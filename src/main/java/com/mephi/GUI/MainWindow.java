package com.mephi.GUI;

import com.mephi.Data.*;
import com.mephi.Parsers.PresentationParser;
import com.mephi.Settings.Settings;
import com.mephi.Threads.AttachmentQueueThread;
import com.mephi.Threads.ExcelReportsThread;
import com.mephi.Threads.MailResponseThread;
import com.mephi.Threads.PresentationInitThread;
import com.sun.istack.internal.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import javax.mail.*;
import javax.mail.internet.MimeUtility;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class MainWindow extends JFrame {

    private JPanel JPanel;
    private JTextField textFieldFileName;
    private JTextField textFieldExtension;
    private JButton startButton;
    private JProgressBar progressBar1;
    private JComboBox<String> comboBoxAccount;
    private JTextPane filePathPane1;
    private JLabel progress;
    private JTabbedPane tabbedPane1;
    private JList<String> listMails;
    private JTextField textFieldHostImap;
    private JTextField textFieldPortImap;
    private JTextField textFieldPortSmtp;
    private JTextField textFieldLogin;
    private JTextField textFieldPassword;
    private JCheckBox checkBoxSSL;
    private JTextField textFieldLabel;
    private JButton buttonCreateNewMail;
    private JButton buttonDeleteMail;
    private JButton saveEmailButton;
    private JTextPane filePathPane2;
    private JButton buttonSetFolder;
    private JPanel panel_space_settings;
    private JCheckBox checkBoxCreateExcel;
    private JCheckBox checkBoxResponse;
    private JTextPane textPaneTextResponse;
    private JTextField textFieldHostSmtp;
    private JList<FolderV1> listFolders;

    private List<Thread> threadsList = new ArrayList<>();

    public MainWindow() {
        folderIgnore.add("drafts");
        folderIgnore.add("outbox");
        folderIgnore.add("sent");
        folderIgnore.add("spam");
        folderIgnore.add("trash");

        setContentPane(JPanel);
        pack();
        setVisible(true);
        setLocation(100, 100);
        Dimension sSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize((int) (sSize.getWidth() * 0.7), (int) (sSize.getHeight() * 0.7));

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                String str = (String) comboBoxAccount.getSelectedItem();
                if (StringUtils.isNotBlank(str)) {
                    Settings.setProperty("defMail", str);
                } else {
                    Settings.deleteProperty("defMail");
                }
                if (StringUtils.isNotBlank(textFieldFileName.getText())) {
                    Settings.setProperty("defFileExists", textFieldFileName.getText());
                } else {
                    Settings.deleteProperty("defFileExists");
                }
                if (StringUtils.isNotBlank(textFieldExtension.getText())) {
                    Settings.setProperty("defFileExt", textFieldExtension.getText());
                } else {
                    Settings.deleteProperty("defFileExt");
                }
                if (StringUtils.isNotBlank(textPaneTextResponse.getText())) {
                    Settings.setProperty(Settings.mailTextResponse,textPaneTextResponse.getText());
                } else {
                    Settings.deleteProperty(Settings.mailTextResponse);
                }
                Settings.setProperty(Settings.createExcel, String.valueOf(checkBoxCreateExcel.isSelected()));
                Settings.setProperty(Settings.sendResponse, String.valueOf(checkBoxResponse.isSelected()));
                Settings.saveAll();
                super.windowClosing(e);
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {
                String str = (String) comboBoxAccount.getSelectedItem();
                if (StringUtils.isNotBlank(str)) {
                    Settings.setProperty("defMail", str);
                } else {
                    Settings.deleteProperty("defMail");
                }
                if (StringUtils.isNotBlank(textFieldFileName.getText())) {
                    Settings.setProperty("defFileExists", textFieldFileName.getText());
                } else {
                    Settings.deleteProperty("defFileExists");
                }
                if (StringUtils.isNotBlank(textFieldExtension.getText())) {
                    Settings.setProperty("defFileExt", textFieldExtension.getText());
                } else {
                    Settings.deleteProperty("defFileExt");
                }
                if (StringUtils.isNotBlank(textPaneTextResponse.getText())) {
                    Settings.setProperty(Settings.mailTextResponse,textPaneTextResponse.getText());
                } else {
                    Settings.deleteProperty(Settings.mailTextResponse);
                }
                Settings.setProperty(Settings.createExcel, String.valueOf(checkBoxCreateExcel.isSelected()));
                Settings.setProperty(Settings.sendResponse, String.valueOf(checkBoxResponse.isSelected()));
                Settings.saveAll();
                super.windowClosed(e);
                System.exit(0);
            }
        });

        tabbedPane1.addChangeListener(e -> {
            if (tabbedPane1.getTitleAt(tabbedPane1.getSelectedIndex()).equalsIgnoreCase("Основное меню")) {
                System.out.print("");
            } else {
                if (wait) {
                    tabbedPane1.setSelectedIndex(0);
                }
            }
        });

        startButton.addActionListener((event) -> {
            if (wait) {
                if (startButton.getText().equalsIgnoreCase("Стоп")) {
                    threadsList.stream()
                            .filter(x -> !x.getName().equalsIgnoreCase("downLoadAttachmentStart"))
                            .forEach(Thread::interrupt);
//                    threadsList.stream()
//                            .filter(x -> !x.getName().equalsIgnoreCase("downLoadAttachmentStart"))
//                            .forEach(x -> {
//                                try {
//                                    x.join();
//                                } catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//                            });
                    threadsList.stream()
                            .filter(x-> x.getName().equalsIgnoreCase("downLoadAttachmentStart"))
                            .filter(Thread::isAlive)
                            .collect(Collectors.toSet())
                            .forEach(x-> {
                                x.interrupt();
                                try {
                                    x.join();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            });
                    threadsList.clear();
                    progress.setText("");
                    startButton.setText("Запуск");
                    comboBoxAccount.setEnabled(true);
                    textFieldFileName.setEnabled(true);
                    textFieldExtension.setEnabled(true);
                    checkBoxCreateExcel.setEnabled(true);
                    checkBoxResponse.setEnabled(true);
                    filePathPane1.setEnabled(true);
                    progressBar1.setValue(0);
                    wait = false;
                }
                return;
            }
            if (listFolders.getSelectedIndices().length == 0) {
                JOptionPane.showMessageDialog(MainWindow.this, "Выберите папки для загрузки", "Ошибка!", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (StringUtils.isBlank(Settings.getProperty("folder", ""))) {
                JFileChooser fileDialog = new JFileChooser();
                fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileDialog.setDialogType(JFileChooser.SAVE_DIALOG);
                fileDialog.setDialogTitle("Выбрать папку для сохранения вложений");
                int ret = fileDialog.showDialog(MainWindow.this, "Выбрать папку");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    Thread thread = new Thread(() -> {
                        setFilePathToLabel(fileDialog.getSelectedFile().getPath());
                        downLoadAttachmentStart(fileDialog.getSelectedFile().getPath());
                    });
                    thread.setName("downLoadAttachmentStart");
                    threadsList.add(thread);
                    thread.start();
                }
            } else {
                Thread thread = new Thread(() -> {
                    String path = Settings.getProperty("folder", "./attachments");
                    downLoadAttachmentStart(path);
                });
                thread.setName("downLoadAttachmentStart");
                threadsList.add(thread);
                thread.start();
            }
        });

        filePathPane1.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileDialog = new JFileChooser();
                fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileDialog.setDialogType(JFileChooser.SAVE_DIALOG);
                fileDialog.setDialogTitle("Выбрать папку для сохранения вложений");
                if (StringUtils.isNotBlank(Settings.getProperty("folder", ""))) {
                    try {
                        File file = new File(Settings.getProperty("folder", ""));
                        fileDialog.setSelectedFile(file);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                int ret = fileDialog.showDialog(MainWindow.this, "Выбрать папку");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    setFilePathToLabel(fileDialog.getSelectedFile().getPath());
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        filePathPane2.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileDialog = new JFileChooser();
                fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileDialog.setDialogType(JFileChooser.SAVE_DIALOG);
                fileDialog.setDialogTitle("Выбрать папку для сохранения вложений");
                if (StringUtils.isNotBlank(Settings.getProperty("folder", ""))) {
                    try {
                        File file = new File(Settings.getProperty("folder", ""));
                        fileDialog.setSelectedFile(file);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                int ret = fileDialog.showDialog(MainWindow.this, "Выбрать папку");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    setFilePathToLabel(fileDialog.getSelectedFile().getPath());
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        buttonSetFolder.addActionListener(e -> {
            JFileChooser fileDialog = new JFileChooser();
            fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileDialog.setDialogType(JFileChooser.SAVE_DIALOG);
            fileDialog.setDialogTitle("Выбрать папку для сохранения вложений");
            if (StringUtils.isNotBlank(Settings.getProperty("folder", ""))) {
                try {
                    File file = new File(Settings.getProperty("folder", ""));
                    fileDialog.setSelectedFile(file);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            int ret = fileDialog.showDialog(MainWindow.this, "Выбрать папку");
            if (ret == JFileChooser.APPROVE_OPTION) {
                setFilePathToLabel(fileDialog.getSelectedFile().getPath());
            }
        });

        updateSelected();

        String mail = Settings.getProperty("defMail", "");
        if (StringUtils.isNotBlank(mail)) {
            for (int i = 0; i < comboBoxAccount.getItemCount(); i++) {
                if (comboBoxAccount.getItemAt(i).equals(mail)) {
                    comboBoxAccount.setSelectedIndex(i);
                    break;
                }
            }
        }

        textFieldFileName.setText(Settings.getProperty("defFileExists", ""));

        textFieldExtension.setText(Settings.getProperty("defFileExt", "ppt pptx"));

        setFilePathToLabel(Settings.getProperty("folder", ""));

        initMailSettings();

        progress.setText("");

        checkBoxCreateExcel.setSelected(Settings.getPropertyBoolean(Settings.createExcel, false));
        checkBoxResponse.setSelected(Settings.getPropertyBoolean(Settings.sendResponse, false));

        textPaneTextResponse.setText(Settings.getProperty(Settings.mailTextResponse,"Уважаемый коллега! \nПросьба оформить название файла в соответствии с шаблоном. \nШаблон можно получить у старосты Вашей группы. \n\nС уважением, кафедра философии."));

        Value<Boolean> selectedAllFolder = new Value<>(false);

        listFolders.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (updateSelectedMethodRun) {
                    return;
                }
                if (listFolders.isSelectedIndex(0) && selectedAllFolder.getValue() == false) {
                    selectedAllFolder.setValue(true);
                    int n = listFolders.getModel().getSize();
                    listFolders.setSelectionInterval(0,n-1);
                } else if (listFolders.isSelectedIndex(0) == false && selectedAllFolder.getValue()) {
                    selectedAllFolder.setValue(false);
                } else if (listFolders.isSelectedIndex(0)) {
                    selectedAllFolder.setValue(true);
                    int n = listFolders.getModel().getSize();
                    listFolders.setSelectionInterval(0,n-1);
                }
            }
        });
        comboBoxAccount.addActionListener((event)->{
            if (updateSelectedMethodRun) {
                return;
            }
            System.out.println(comboBoxAccount.getSelectedIndex());
            generateFoldersViews();
        });
        comboBoxAccount.setSelectedIndex(comboBoxAccount.getSelectedIndex());
    }

    private void generateFoldersViews() {
        String jLabel = (String) comboBoxAccount.getSelectedItem();

        JSONObject jsonObject = new JSONObject(Settings.getProperty("emails", "{}"));

        JSONObject mailParams;

        if (jLabel != null && jsonObject.has(jLabel)) {
            mailParams = jsonObject.getJSONObject(jLabel);
        } else {
            return;
        }

        System.out.println(mailParams.getString("login") + " " + mailParams.getString("password"));

        Authenticator auth = new MyAuthenticator(mailParams.getString("login"), mailParams.getString("password"));

        Properties props = System.getProperties();
        props.put("mail.user", mailParams.getString("login"));
        props.put("mail.host", mailParams.get("hostImap"));
        props.put("mail.store.protocol", "imap");
        props.put("mail.debug", "false");

        props.put("mail.imap.partialfetch", "false");
        props.put("mail.imaps.partialfetch", "false");

        props.put("mail.imap.ssl.enable", String.valueOf(mailParams.getBoolean("ssl")).toLowerCase());
        props.put("mail.imap.port", mailParams.getInt("portImap"));
        props.put("mail.transport.protocol", "smtp");

        props.remove("mail.smtp.host");
        props.remove("mail.smtp.port");
        props.remove("mail.smtp.ssl.enable");
        props.remove("mail.smtp.login");
        props.remove("mail.smtp.password");
        props.remove("mail.smtp.auth");

        props.put("mail.imap.ssl.trust", "*");

        Session session = Session.getInstance(props,auth);
        try {
            Store store = session.getStore();
            try {
                store.connect();
                List<Folder> list = getChildrenFolder(store.getDefaultFolder());
                list.removeIf(x-> StringUtils.isBlank(x.getName()));
                list.removeIf(x -> folderIgnore.contains(x.getName().toLowerCase()));
//                list.removeIf(x -> {
//                    try {
//                        return x.getMessageCount() < 1;
//                    } catch (MessagingException e) {
//                        e.printStackTrace();
//                    }
//                    return false;
//                });
                System.out.println(list.size());
                list.forEach(x-> System.out.println(x.getName()));

                ListModelFolderV1 listModelFolderV1 = new ListModelFolderV1(new ArrayList<>());
                listModelFolderV1.getList().add(new FolderV1("Все папки","",0));
                listModelFolderV1.getList().get(0).setAllFolders(true);
                list.parallelStream().map(x-> {
                    try {
                        return new FolderV1(x.getName(),x.getFullName(),x.getMessageCount());
                    } catch (MessagingException e) {
                        return new FolderV1(x.getName(),x.getFullName(),-1);
                    }
                }).forEachOrdered(x-> listModelFolderV1.getList().add(x));
                listFolders.setModel(listModelFolderV1);

//                listModel.addElement("Все папки!");
//                for (Folder item : list) {
//                    listModel.addElement(item.getName());
//                }
//                listFolders.setModel(listModel);

            } finally {
                store.close();
            }
        } catch (javax.mail.AuthenticationFailedException e) {
            JOptionPane.showMessageDialog(MainWindow.this, "Ошибка авторизации.\nПроверьте настройки.\nЕсли вы используйете gmail,\nто необходимо разрешить доступ для небезопасных приложений\nпо ссылке https://myaccount.google.com/lesssecureapps\nПолный текст ошибки авторизации:\n" + e.getMessage(), "Ошибка!", JOptionPane.ERROR_MESSAGE);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private List<Folder> getChildrenFolder(Folder folder) {
        List<Folder> list = new ArrayList<>();
        if (folder == null) {
            return list;
        }
        if (StringUtils.isNotBlank(folder.getName())) {
            for (String item : folderIgnore) {
                if (folder.getName().toLowerCase().equalsIgnoreCase(item.toLowerCase())) {
                    return list;
                }
            }
            list.add(folder);
        }
        try {
            if (folder.list() != null) {
                for (Folder item : folder.list()) {
                    list.addAll(getChildrenFolder(item));
                }
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return list;
    }

    private boolean updateSelectedMethodRun = false;

    private void updateSelected() {
        updateSelectedMethodRun = true;
        String selectedValue = (String) comboBoxAccount.getSelectedItem();
        comboBoxAccount.removeAllItems();
        JSONObject jsonObject = new JSONObject(Settings.getProperty("emails", "{}"));
        jsonObject.keySet().stream().sorted().forEach(x -> comboBoxAccount.addItem(x));
        updateSelectedMethodRun = false;
        if (comboBoxAccount.getItemCount() > 0) {
            int index = 0;
            for (int i = 0; i < comboBoxAccount.getItemCount(); i++) {
                System.out.println(comboBoxAccount.getItemAt(i));
                System.out.println(selectedValue);
                if (comboBoxAccount.getItemAt(i).equalsIgnoreCase(selectedValue)) {
                    index = i;
                    break;
                }
            }
            if (comboBoxAccount.getSelectedIndex() != index) {
                comboBoxAccount.setSelectedIndex(index);
            }
        } else {
            if (comboBoxAccount.getSelectedIndex() != -1) {
                comboBoxAccount.setSelectedIndex(-1);
            }
        }
    }

    private void downLoadAttachmentStart(String path) {
        wait = true;
        comboBoxAccount.setEnabled(false);
        textFieldFileName.setEnabled(false);
        textFieldExtension.setEnabled(false);
        checkBoxCreateExcel.setEnabled(false);
        checkBoxResponse.setEnabled(false);
        filePathPane1.setEnabled(false);

        startButton.setText("Стоп");
        try {
            downLoadAttachment(path);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(MainWindow.this, "При выполнении операции произошла неизвестная ошибка\n" + e.toString(), "Ошибка", JOptionPane.ERROR_MESSAGE);
        }
        threadsList.stream().filter(x-> !x.equals(Thread.currentThread())).peek(Thread::interrupt).forEach(x-> {
            try {
                x.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        threadsList.clear();
        progressBar1.setValue(0);
        startButton.setText("Запуск");
        comboBoxAccount.setEnabled(true);
        textFieldFileName.setEnabled(true);
        textFieldExtension.setEnabled(true);
        checkBoxCreateExcel.setEnabled(true);
        checkBoxResponse.setEnabled(true);
        filePathPane1.setEnabled(true);
        wait = false;
    }

    private List<String> folderIgnore = new ArrayList<>();

    private boolean wait = false;

    private void downLoadAttachment(String path) {

        if (comboBoxAccount.getItemCount() == 0) {
            JOptionPane.showMessageDialog(MainWindow.this, "Настройте почту", "Ошибка! Нет почтовых адресов", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String jLabel = (String) comboBoxAccount.getSelectedItem();

        JSONObject jsonObject = new JSONObject(Settings.getProperty("emails", "{}"));

        JSONObject mailParams;

        if (jLabel != null && jsonObject.has(jLabel)) {
            mailParams = jsonObject.getJSONObject(jLabel);
        } else {
            JOptionPane.showMessageDialog(MainWindow.this, "Настройте почту", "Ошибка!", JOptionPane.ERROR_MESSAGE);
            return;
        }

        System.out.println(mailParams.getString("login") + " " + mailParams.getString("password"));

        Authenticator auth = new MyAuthenticator(mailParams.getString("login"), mailParams.getString("password"));

        Properties props = System.getProperties();
        props.put("mail.user", mailParams.getString("login"));
        props.put("mail.host", mailParams.get("hostImap"));
        props.put("mail.debug", "false");
        props.put("mail.store.protocol", "imap");

        props.put("mail.imap.partialfetch", "false");
        //props.put("mail.imap.fetchsize", BinaryByteUnit.MEBIBYTES.toBytes(100) + "");
        props.put("mail.imaps.partialfetch", "false");
        //props.put("mail.imaps.fetchsize", BinaryByteUnit.MEBIBYTES.toBytes(100) + "");

        props.put("mail.imap.ssl.enable", String.valueOf(mailParams.getBoolean("ssl")).toLowerCase());
        props.put("mail.imap.port", mailParams.getInt("portImap"));
        props.put("mail.transport.protocol", "smtp");

        if (checkBoxResponse.isSelected()) {
            if (!mailParams.has("hostSmtp") || !mailParams.has("portSmtp")) {
                JOptionPane.showMessageDialog(MainWindow.this, "Отсутствует параметр smtp сервера.\nПерейдите в настройки почты. (6)", "Ошибка", JOptionPane.ERROR_MESSAGE);
                return;
            }
            props.put("mail.smtp.host",mailParams.getString("hostSmtp"));
            props.put("mail.smtp.port", mailParams.getInt("portSmtp"));
            props.put("mail.smtp.ssl.enable", String.valueOf(mailParams.getBoolean("ssl")).toLowerCase());
            props.put("mail.smtp.login",mailParams.getString("login"));
            props.put("mail.smtp.password",mailParams.getString("password"));
            props.put("mail.smtp.auth","true");

            props.put("mail.smtp.ssl.trust", "*");
        } else {
            props.remove("mail.smtp.host");
            props.remove("mail.smtp.port");
            props.remove("mail.smtp.ssl.enable");
            props.remove("mail.smtp.login");
            props.remove("mail.smtp.password");
            props.remove("mail.smtp.auth");

            props.remove("mail.smtp.ssl.trust");
        }

        props.put("mail.imap.ssl.trust", "*");

        progressBar1.setValue(1);
        progress.setText("Инициализация сессии");
        Session session = Session.getInstance(props, auth);
        Store store;
        progress.setText("Инициализация удаленного хранилища");
        try {
            store = session.getStore();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(MainWindow.this, "Ошибка при установке соединения (1)\n" + e.toString(), "Ошибка", JOptionPane.ERROR_MESSAGE);
            return;
        }
        progressBar1.setValue(2);
        progress.setText("Подключение к удаленному хранилищу");
        try {
            store.connect();
        } catch (MessagingException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(MainWindow.this, "Ошибка при установке соединения (2)\n" + e.toString(), "Ошибка", JOptionPane.ERROR_MESSAGE);
            return;
        }
        progressBar1.setValue(3);
        progress.setText("Получение списка директорий почты");
        List<Folder> folderList;
        try {
//            for (Folder item : store.getDefaultFolder().list()) {
//                if (folderIgnore.contains(item.getName().toLowerCase())) {
//                    continue;
//                }
//                if (folderIgnore.contains(item.getFullName().toLowerCase())) {
//                    continue;
//                }
//                folderList.add(item);
//            }

            folderList = getChildrenFolder(store.getDefaultFolder()).stream()
                    .filter(x-> StringUtils.isNotBlank(x.getName()))
                    .filter(x-> !folderIgnore.contains(x.getName().toLowerCase()))
                    .filter(x-> {
                        for (FolderV1 item : listFolders.getSelectedValuesList()) {
                            if (item.isAllFolders()) {
                                continue;
                            }
                            if (StringUtils.equals(x.getName(),item.getName()) == false) {
                                continue;
                            }
                            if (StringUtils.equals(x.getFullName(),item.getFullname()) == false) {
                                continue;
                            }
                            return true;
                        }
                        return false;
                    }).collect(Collectors.toList());


//            folderList.removeIf(x-> StringUtils.isBlank(x.getName()));
//            folderList.removeIf(x-> folderIgnore.contains(x.getName().toLowerCase()));
//            ListModelFolderV1 listModelFolderV1 = (ListModelFolderV1) listFolders.getModel();
//            folderList.removeIf(x-> listModelFolderV1.getList().stream().filter(x2-> !x2.isAllFolders()).filter(x2-> !x2.isAllFolders()).noneMatch(x2-> StringUtils.equals(x.getName(),x2.getName()) && StringUtils.equals(x.getFullName(),x2.getFullname())));

        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(MainWindow.this, "Ошибка при установке соединения (3)\n" + e.toString(), "Ошибка", JOptionPane.ERROR_MESSAGE);
            return;
        }
        progressBar1.setValue(4);
        progress.setText("Получение доступа к содержимому папок");
        try {
            for (Folder folder : folderList) {
                folder.open(Folder.READ_ONLY);
            }
        } catch (MessagingException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(MainWindow.this, "Ошибка при установке соединения (4)\n" + e.toString(), "Ошибка", JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<Message> messageList = new ArrayList<>();
        progress.setText("Получение списка писем из директорий");
        try {
            for (Folder item : folderList) {
                try {
                    progress.setText("Получение списка писем из директории " + item.getName());
                } catch (Exception ignored) {

                }
                messageList.addAll(Arrays.asList(item.getMessages()));
            }
        } catch (MessagingException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(MainWindow.this, "Ошибка при установке соединения (5)\n" + e.toString(), "Ошибка", JOptionPane.ERROR_MESSAGE);
            return;
        }

        progressBar1.setValue(5);
        progress.setText("Подготовка к обработке сообщений 1 из 3 этапов");
        progressBar1.setMaximum(messageList.size());
        String filePart = textFieldFileName.getText().toLowerCase();
        String[] fileExt = textFieldExtension.getText().toLowerCase().split(" ");
        LinkedBlockingQueue<PresentationParser> presentations;
        LinkedBlockingQueue<Presentation_V1> presentationsForSave;
        LinkedBlockingQueue<MessageData> messagesData;
        LinkedBlockingQueue<AttachmentData> attachmentsQueue = new LinkedBlockingQueue<>();
        if (checkBoxCreateExcel.isSelected()) {
            presentations = new LinkedBlockingQueue<>();
            presentationsForSave = new LinkedBlockingQueue<>();
            ExcelReportsThread excelReportsThread = new ExcelReportsThread(path, presentationsForSave, MainWindow.this);
            Thread threadExcel = new Thread(excelReportsThread);
            threadExcel.setName("threadExcel");
            threadsList.add(threadExcel);
            threadExcel.start();
            Value<Integer> k = new Value<>(1);
            for (int i=0;i<2;i++) {
                PresentationInitThread presentationInitThread = new PresentationInitThread(k,presentations,presentationsForSave);
                Thread thread = new Thread(presentationInitThread);
                thread.setName("presentationInitThread " + i);
                threadsList.add(thread);
                thread.start();
            }
        } else {
            presentations = null;
            presentationsForSave = null;
        }
        progress.setText("Подготовка к обработке сообщений 2 из 3 этапов");
        if (checkBoxResponse.isSelected()) {
            messagesData = new LinkedBlockingQueue<>();
            MailResponseThread mailResponseThread = new MailResponseThread(session,jLabel,textPaneTextResponse.getText(),messagesData);
            Thread thread = new Thread(mailResponseThread);
            thread.setName("mailResponseThread");
            threadsList.add(thread);
            thread.start();
        } else {
            messagesData = null;
        }
        progress.setText("Подготовка к обработке сообщений 3 из 3 этапов");
        Value<Integer> k = new Value<>(1);
        for (int i=0;i<2;i++) {
            AttachmentQueueThread attachmentQueueThread = new AttachmentQueueThread(k, attachmentsQueue, presentations);
            Thread thread = new Thread(attachmentQueueThread);
            thread.setName("attachmentQueueThread " + i);
            threadsList.add(thread);
            thread.start();
        }

        for (int i = 0; i < messageList.size(); i++) {
            if (Thread.interrupted()) {
                return;
            }
            progressBar1.setValue(i);
            progress.setText("Обработано " + (i) + " из " + messageList.size());
            try {
                System.out.println("Запущена обработка " + (i+1) + " сообщения");
                saveFilesFromMessage(messageList.get(i), path, filePart, fileExt, attachmentsQueue, messagesData);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                System.out.println("Завершена обработка " + (i+1) + " сообщения");
            }
            progressBar1.setValue(i + 1);
            progress.setText("Обработано " + (i + 1) + " из " + messageList.size());
        }
//        Value<Integer> progressNumber = new Value<>(0);
//        messageList.parallelStream().forEach(x-> {
//            synchronized (progressNumber) {
//                progressBar1.setValue(progressNumber.getValue());
//                progress.setText("Обработано " + progressNumber.getValue() + " из " + messageList.size());
//            }
//            try {
//                saveFilesFromMessage(x, path, filePart, fileExt, presentations, messagesData);
//            } catch (MessagingException | IOException e) {
//                e.printStackTrace();
//            }
//            synchronized (progressNumber) {
//                progressNumber.setValue(progressNumber.getValue() + 1);
//                progressBar1.setValue(progressNumber.getValue());
//                progress.setText("Обработано " + progressNumber.getValue() + " из " + messageList.size());
//            }
//        });

        progressBar1.setMaximum(100);
        progressBar1.setValue(90);

        progress.setText("Почти закончил");
        while (attachmentsQueue.size() > 0) {
            progress.setText("Осталось обработать " + attachmentsQueue.size() + " файлов");
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {

            }
        }

        progressBar1.setValue(93);
        progress.setText("Почти закончил.");
        while ((presentations != null && presentations.size() > 0) || (presentationsForSave != null && presentationsForSave.size() > 0)) {
            try {
                int i = 0;
                if (presentations != null) {
                    i += presentations.size();
                }
                if (presentationsForSave != null) {
                    i += presentationsForSave.size();
                }
                progress.setText("Осталось обработать " + i + " презентаций");
                Thread.sleep(300);
            } catch (InterruptedException ignored) {

            }
        }
//        while (presentationsForSave != null && presentationsForSave.size() > 0) {
//            try {
//                progress.setText("Осталось обработать " + presentations.size() + " презентаций");
//                Thread.sleep(300);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        progressBar1.setValue(96);
        progress.setText("Почти закончил..");
        while (messagesData != null && messagesData.size() > 0) {
            try {
                progress.setText("Осталось отправить " + messagesData.size() + " ответов");
                Thread.sleep(300);
            } catch (InterruptedException ignored) {

            }
        }
        progressBar1.setValue(99);
        progress.setText("Почти закончил...");
        threadsList.stream().filter(x-> x.equals(Thread.currentThread()) == false).forEach(Thread::interrupt);
        threadsList.stream().filter(x-> x.equals(Thread.currentThread()) == false).forEach(x-> {
            try {
                x.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
//        threadsList.stream().filter(x-> !x.equals(Thread.currentThread())).peek(Thread::interrupt).forEach(x-> {
//            try {
//                x.join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });
        progressBar1.setValue(100);
        progress.setText("Обработка завершена");
        System.out.println("Обработка завершена");


        JOptionPane.showMessageDialog(MainWindow.this, "Вложения почты сохранены в указанную вами директорию!", "Done", JOptionPane.INFORMATION_MESSAGE);

//        try {
//            inbox.setFlags(messages, new Flags(Flags.Flag.SEEN), true);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }

        folderList.forEach(x -> {
            try {
                x.close();
            } catch (MessagingException ignored) {

            }
        });
        try {
            store.close();
        } catch (MessagingException ignored) {

        }
    }

    private void saveFilesFromMessage(@NotNull Message inMessage, String path, String filePart, String[] fileExt, Queue<AttachmentData> attachmentDataQueue, Queue<MessageData> messagesData) throws MessagingException, IOException {
        if (inMessage.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) inMessage.getContent();
            boolean sendResp = false;
            int n = mp.getCount();
            for (int i = 0; i < n; i++) {
                Part part = mp.getBodyPart(i);
                String fileNameEncoded = part.getFileName();
                int sizeOfPart = part.getSize();
                String dis = part.getDisposition();
                if (fileNameEncoded != null && sizeOfPart > 0 && Part.ATTACHMENT.equalsIgnoreCase(dis)) {
                    String filename = MimeUtility.decodeText(part.getFileName());
                    if (checkFileName(filePart, filename)) {
                        if (checkExt(filename, fileExt)) {
                            saveFile(path, filename, part, attachmentDataQueue);
                        } else {
                            sendResp = true;
                        }
                    }
                    if (messagesData != null && StringUtils.isNotBlank(filename) && !sendResp) {
                        sendResp = !checkFileNamePattern(filename);
                    }
                }
            }
            if (sendResp && messagesData != null) {
                for (Address item : inMessage.getFrom()) {
                    MessageData messageData = new MessageData();
                    messageData.setTitle(inMessage.getSubject());
                    messageData.setTo(item.toString());
                    messageData.setText("");
                    messagesData.add(messageData);
                }
            }
        }
    }

    private boolean checkFileNamePattern(String filename) {
        /*
        * Я могу проверить наличие 5 групп «тегов»:
2019 – год
Все символы цифры?
met – я так понимаю это курс, по которому презентация
Есть только буквы?
M17_110 – номер группы
Номер группы из двух частей?
Есть только буквы и цифры?
KamitovaVV – ФИО студента
Только буквы?
synergetica – наименование темы
Только буквы и цифры?

        2019_met_M17_110_KamitovaVV_synergetica.pptx* */
        if (StringUtils.isBlank(filename))
            return false;
        int dot = filename.lastIndexOf('.');
        if (dot < 1) {
            return false;
        }
        filename = filename.substring(0,dot - 1);
        String[] mass = filename.split("_");
        if (mass.length != 6) {
            return false;
        }
        if (!StringUtils.isNumeric(mass[0])) {
            return false;
        }
        if (isOnlyLetter(mass[1])) {
            return false;
        }
        if (isOnlyLetterAndNumber(mass[2])) {
            return false;
        }
        if (isOnlyLetterAndNumber(mass[3])) {
            return false;
        }
        if (isOnlyLetter(mass[4])) {
            return false;
        }
        //noinspection RedundantIfStatement
        if (isOnlyLetterAndNumber(mass[5])) {
            return false;
        }

        return true;
    }

    private boolean isOnlyLetterAndNumber(String str) {
        if (str == null)
            return false;
        int n = str.length();
        if (n == 0)
            return false;
        for (int i = 0; i < n; i++) {
            if (!Character.isLetterOrDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean isOnlyLetter(String str) {
        if (str == null)
            return false;
        int n = str.length();
        if (n == 0)
            return false;
        for (int i = 0; i < n; i++) {
            if (!Character.isLetter(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean checkFileName(String filePart, String filename) {
        if (StringUtils.isBlank(filePart)) {
            return true;
        }
        if (!filename.toLowerCase().contains(filePart.toLowerCase())) {
            System.out.println("Имя не подошло");
            return false;
        }
        return true;
    }

    private boolean checkExt(String filename, String... fileExt) {
        if (fileExt == null) {
            return true;
        }
        if (fileExt.length == 0) {
            return true;
        }
        if (Arrays.stream(fileExt).allMatch(StringUtils::isBlank)) {
            return true;
        }
        int dot = filename.lastIndexOf(".");
        if (dot != -1) {
            String str = filename.substring(dot).toLowerCase();
            if (str.startsWith(".") && str.length() > 1)
                str = str.substring(1);
            for (String ext : fileExt) {
                if (str.equalsIgnoreCase(ext)) {
                    return true;
                }
            }
        } else {
            String str = filename.toLowerCase();
            for (String ext : fileExt) {
                if (str.endsWith(ext)) {
                    return true;
                }
            }
        }
        StringBuilder message = new StringBuilder("Окончание не подошло. filename - " + filename + ". exts: ");
        int n = fileExt.length;
        for (int i = 0; i < n; i++) {
            message.append(fileExt[i]);
            if (i + 1 < n)
                message.append(", ");
        }
        message.append(".");
        System.out.println(message.toString());
        return false;
    }

    private String saveFile(@NotNull String dir, String filename, @NotNull Part input, Queue<AttachmentData> blockingQueue) {
        String result = null;
        try {
            System.out.println("Получение файла для сохранения " + filename);
//            Object content = input.getContent();
//            InputStream inputStream = (InputStream) content;
            if (!dir.endsWith("/")) {
                dir += "/";
            }
            //noinspection ResultOfMethodCallIgnored
            new File(dir).mkdirs();
            String path = dir + filename;
            File file = new File(path);
            int i = 1;
            while (file.exists()) {
                int dot = filename.lastIndexOf(".");
                if (dot != -1) {
                    String ext = filename.substring(dot);
                    String name = filename.substring(0, dot);
                    path = dir + name + "(" + i + ")" + "" + ext;
                    file = new File(path);
                } else if (filename.length() > 0) {
                    path = dir + filename + "(" + i + ")";
                    file = new File(path);
                    System.out.println("filename - " + filename);
                } else {
                    return null;
                }
                i++;
            }
            file = new File(path);
            result = path;
            if (!file.createNewFile()) {
                throw new IOException("Не могу создать файл " + path);
            }
            AttachmentData attachmentData = new AttachmentData(path,file,null,input);
            blockingQueue.add(attachmentData);
//            FileOutputStream out = new FileOutputStream(file, false);
//            org.apache.commons.io.IOUtils.copyLarge(inputStream, out);
//            inputStream.close();
//            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Получение файла для сохранения завершено " + filename);
        return result;
    }

    private void updateListEmails() {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        String str = Settings.getProperty("emails", "{}");
        JSONObject emails = new JSONObject(str);
        emails.keySet().stream().sorted().forEach(listModel::addElement);
        listMails.setModel(listModel);
    }

    private void initMailSettings() {
        updateListEmails();

        listMails.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        listMails.addListSelectionListener(e -> {
            String jLabel = listMails.getSelectedValue();
            if (jLabel == null)
                return;
            String selectedEmail = jLabel + "";
            String str = Settings.getProperty("emails", "{}");
            JSONObject emails = new JSONObject(str);
            JSONObject jsonObject = emails.getJSONObject(jLabel);
            textFieldLabel.setText(selectedEmail);
            if (jsonObject.has("hostImap")) {
                textFieldHostImap.setText(jsonObject.getString("hostImap"));
            } else {
                textFieldHostImap.setText("");
            }
            if (jsonObject.has("hostSmtp")) {
                textFieldHostSmtp.setText(jsonObject.getString("hostSmtp"));
            } else {
                textFieldHostSmtp.setText("");
            }
            if (jsonObject.has("portImap")) {
                textFieldPortImap.setText(jsonObject.getString("portImap"));
            } else {
                textFieldPortImap.setText("");
            }
            if (jsonObject.has("portSmtp")) {
                textFieldPortSmtp.setText(jsonObject.getString("portSmtp"));
            } else {
                textFieldPortSmtp.setText("");
            }
            if (jsonObject.has("login")) {
                textFieldLogin.setText(jsonObject.getString("login"));
            } else {
                textFieldLogin.setText("");
            }
            if (jsonObject.has("password")) {
                textFieldPassword.setText(jsonObject.getString("password"));
            } else {
                textFieldPassword.setText("");
            }
            if (jsonObject.has("ssl")) {
                checkBoxSSL.setSelected(jsonObject.getBoolean("ssl"));
            } else {
                checkBoxSSL.setSelected(false);
            }
        });


        buttonCreateNewMail.addActionListener(e -> {
            listMails.removeSelectionInterval(listMails.getSelectedIndex(), listMails.getSelectedIndex());
            clearEmailFields();
        });

        buttonDeleteMail.addActionListener(e -> {
            String str = Settings.getProperty("emails", "{}");
            JSONObject emails = new JSONObject(str);
            if (listMails.getSelectedIndex() != -1) {
                emails.remove(listMails.getSelectedValue());
            }
            Settings.setProperty("emails", emails.toString());
            Settings.saveAll();
            updateListEmails();
            clearEmailFields();
            listMails.removeSelectionInterval(listMails.getSelectedIndex(), listMails.getSelectedIndex());
        });

        saveEmailButton.addActionListener(e -> {
            JSONObject jsonObject = new JSONObject();
            if (StringUtils.isBlank(textFieldLabel.getText())) {
                JOptionPane.showMessageDialog(MainWindow.this, "Не заполнено имя аккаунта.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                return;
            }
            String str = Settings.getProperty("emails", "{}");
            JSONObject emails = new JSONObject(str);

            if (emails.has(textFieldLabel.getText()) && listMails.getSelectedIndex() == -1) {
                JOptionPane.showMessageDialog(MainWindow.this, "Почта с таким аккаунтом уже есть.", "Ошибка!", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (emails.has(textFieldLabel.getText()) && listMails.getSelectedIndex() != -1) {
                if (listMails.getSelectedValue().equals(textFieldLabel.getText())) {
                    System.out.print("");
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "Почта с таким аккаунтом уже есть.", "Ошибка!", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

            if (StringUtils.isNotBlank(textFieldHostImap.getText())) {
                jsonObject.put("hostImap", textFieldHostImap.getText());
            } else {
                JOptionPane.showMessageDialog(MainWindow.this, "Не заполнен host Imap.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (StringUtils.isNotBlank(textFieldPortImap.getText())) {
                if (StringUtils.isNumeric(textFieldPortImap.getText())) {
                    jsonObject.put("portImap", textFieldPortImap.getText());
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "Port imap должен быть числом.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(MainWindow.this, "Не заполнен port imap.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (StringUtils.isNotBlank(textFieldHostSmtp.getText())) {
                jsonObject.put("hostSmtp", textFieldHostSmtp.getText());
            } else {
                if (checkBoxResponse.isSelected()) {
                    JOptionPane.showMessageDialog(MainWindow.this, "Не заполнен host Smtp.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

            if (StringUtils.isNotBlank(textFieldPortSmtp.getText())) {
                if (StringUtils.isNumeric(textFieldPortSmtp.getText())) {
                    jsonObject.put("portSmtp", textFieldPortSmtp.getText());
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "Port smtp должен быть числом.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } else {
                if (checkBoxResponse.isSelected()) {
                    JOptionPane.showMessageDialog(MainWindow.this, "Не заполнен port smtp.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

            if (StringUtils.isNotBlank(textFieldLogin.getText())) {
                jsonObject.put("login", textFieldLogin.getText());
            } else {
                JOptionPane.showMessageDialog(MainWindow.this, "Не заполнен login.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (StringUtils.isNotBlank(textFieldPassword.getText())) {
                jsonObject.put("password", textFieldPassword.getText());
            } else {
                JOptionPane.showMessageDialog(MainWindow.this, "Не заполнен password.", "Ошбика!", JOptionPane.ERROR_MESSAGE);
                return;
            }
            jsonObject.put("ssl", checkBoxSSL.isSelected());

            if (listMails.getSelectedIndex() != -1) {
                emails.remove(listMails.getSelectedValue());
                emails.put(textFieldLabel.getText(), jsonObject);
            } else {
                emails.put(textFieldLabel.getText(), jsonObject);
            }
            Settings.setProperty("emails", emails.toString());
            Settings.saveAll();
            updateSelected();
            if (listMails.getSelectedIndex() != -1 && !listMails.getSelectedValue().equals(textFieldLabel.getText())) {
                if (listMails.getSelectedValue().equals(comboBoxAccount.getSelectedItem())) {
                    comboBoxAccount.setSelectedItem(textFieldLabel.getText());
                }
            }
            listMails.removeSelectionInterval(listMails.getSelectedIndex(), listMails.getSelectedIndex());
            updateListEmails();
            clearEmailFields();
        });
    }

    private void clearEmailFields() {
        textFieldLabel.setText("");
        textFieldHostImap.setText("");
        textFieldHostSmtp.setText("");
        textFieldPortImap.setText("");
        textFieldPortSmtp.setText("");
        textFieldLogin.setText("");
        textFieldPassword.setText("");
        checkBoxSSL.setSelected(false);
    }

    private void setFilePathToLabel(String string) {
        if (StringUtils.isNotBlank(string)) {
            Settings.setProperty("folder", string);

            String html = "<div style='font-size: 20pt; text-align: center; word-break: break-all;'>Сохранять в: " + string + "</div>";
            filePathPane1.setEditable(false);
            filePathPane1.setContentType("text/html");
            filePathPane1.setText(html);

            filePathPane2.setEditable(false);
            filePathPane2.setContentType("text/html");
            filePathPane2.setText(html);
        } else {
            Settings.deleteProperty("folder");
            filePathPane1.setEditable(false);
            filePathPane2.setEditable(false);
            filePathPane1.setContentType("text/plain");
            filePathPane2.setContentType("text/plain");
            filePathPane1.setText("Не выбрана папка для вложений");
            filePathPane2.setText("Не выбрана папка для вложений");
        }
        Settings.saveAll();
    }

    public static class MyAuthenticator extends Authenticator {
        private String user;
        private String password;

        MyAuthenticator(String user, String password) {
            this.user = user;
            this.password = password;
        }

        public PasswordAuthentication getPasswordAuthentication() {
            String user = this.user;
            String password = this.password;
            return new PasswordAuthentication(user, password);
        }
    }
}