package com.mephi.GUI;

import com.mephi.Data.FolderV1;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.util.List;

public class ListModelFolderV1 implements ListModel<FolderV1> {
    private List<FolderV1> list;

    public ListModelFolderV1(List<FolderV1> list) {
        this.list = list;
    }

    public List<FolderV1> getList() {
        return list;
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public FolderV1 getElementAt(int index) {
        return list.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {

    }

    @Override
    public void removeListDataListener(ListDataListener l) {

    }
}
