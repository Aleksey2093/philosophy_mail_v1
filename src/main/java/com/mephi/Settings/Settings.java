package com.mephi.Settings;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Settings {

    public static final String createExcel = "createExcel";
    public static final String sendResponse = "sendResponse";
    public static final String mailTextResponse = "mailTextResponse";

    private static Properties properties;

    static {
        properties = loadSettingFile();
    }

    private static Properties loadSettingFile() {
        Properties property = new Properties();
        try {
            FileInputStream fis = new FileInputStream("./settings.properties");
            property.load(fis);
        } catch (Exception e) {
            System.err.println("ОШИБКА: Файл настроек отсуствует!");
        }
        return property;
    }

    public static Properties getProperties() {
        return properties;
    }

    public static String getProperty(String key, String def) {
        return properties.getProperty(key,def);
    }

    public static boolean getPropertyBoolean(String key, boolean def) {
        return Boolean.parseBoolean(getProperty(key,String.valueOf(def)));
    }

    public static void deleteProperty(String key) {
        properties.remove(key);
    }

    public static void setProperty(String key, String value) {
        properties.setProperty(key,value);
    }

    public static void saveAll() {
        try {
            properties.store(new FileOutputStream("./settings.properties"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    private void loadProps() {
////        Properties properties = loadSettingFile();
////        textFieldHost.setText(properties.getProperty("host","imap.yandex.ru"));
////        textFieldPort.setText(properties.getProperty("port","993"));
////        textFieldSmtp.setText(properties.getProperty("portSMTP","465"));
////        textFieldLogin.setText(properties.getProperty("login",""));
////        textFieldPassword.setText(properties.getProperty("password",""));
////        textFieldFileName.setText(properties.getProperty("filename",""));
////        textFieldExtension.setText(properties.getProperty("ext",""));
////        String t = properties.getProperty("ssl",Boolean.FALSE.toString());
////        try {
////            checkBoxSSL.setSelected(Boolean.parseBoolean(t));
////        } catch (Exception e) {
////            checkBoxSSL.setSelected(false);
////        }
//    }
}
