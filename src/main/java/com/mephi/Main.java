package com.mephi;

import com.mephi.GUI.MainWindow;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        MainWindow mainWindow = new MainWindow();
    }
}
