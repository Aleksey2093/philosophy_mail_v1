package com.mephi.Data;

public class FolderV1 {
    private boolean allFolders;
    private String name;
    private String fullname;
    private int countMessage;

    public FolderV1(String name, String fullname, int countMessage) {
        this.name = name;
        this.fullname = fullname;
        this.countMessage = countMessage;
    }

    public int getCountMessage() {
        return countMessage;
    }

    public String getFullname() {
        return fullname;
    }

    public String getName() {
        return name;
    }

    public void setCountMessage(int countMessage) {
        this.countMessage = countMessage;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAllFolders() {
        return allFolders;
    }

    public void setAllFolders(boolean allFolders) {
        this.allFolders = allFolders;
    }

    @Override
    public String toString() {
        String result = name;
        if (isAllFolders() == false) {
            if (getCountMessage() == -1) {
                result = result + "(?)";
            } else {
                result = result + " (" + getCountMessage() + ")";
            }
        }
        return result;
    }
}
