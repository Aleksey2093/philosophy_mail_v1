package com.mephi.Data;

import javax.mail.MessagingException;
import javax.mail.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class AttachmentData {
    private String path;
    private File file;
    private InputStream inputStream;
    private Part partOfMail;

    public AttachmentData(String path, File file, InputStream inputStream, Part partOfMail) {
        this.path = path;
        this.file = file;
        this.inputStream = inputStream;
        this.partOfMail = partOfMail;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public InputStream getInputStream() {
        if (inputStream == null) {
            try {
                Object content = partOfMail.getContent();
                inputStream = (InputStream) content;
            } catch (IOException | MessagingException e) {
                e.printStackTrace();
            }
        }
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public Part getPartOfMail() {
        return partOfMail;
    }

    public void setPartOfMail(Part partOfMail) {
        this.partOfMail = partOfMail;
    }
}
