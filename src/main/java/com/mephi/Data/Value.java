package com.mephi.Data;

public class Value<T> {
    T value;

    public Value() {

    }

    public Value(T value) {
        setValue(value);
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
