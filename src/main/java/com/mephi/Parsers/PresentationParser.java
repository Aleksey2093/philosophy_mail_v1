package com.mephi.Parsers;

import com.mephi.Data.Presentation_V1;
import com.sun.istack.internal.Nullable;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hslf.usermodel.*;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.sl.usermodel.SlideShow;
import org.apache.poi.xslf.usermodel.*;
import org.apache.xmlbeans.XmlException;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PresentationParser {
    //HSLF	Horrible Slide Layout Format	Компонент чтения и записи файлов PowerPoint, формат PPT
    //XSLF	XML Slide Layout Format	Компонент чтения и записи файлов PowerPoint, формат PPTX

    private XMLSlideShow xmlSlideShow;
    private HSLFSlideShow ppt;
    private InputStream inputStream;
    private int status = 0;
    private String fileName;

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public PresentationParser(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    private void streamReset() {
        if (inputStream instanceof FileInputStream) {
            try {
                File file = new File(fileName);
                if (file.exists()) {
                    try {
                        inputStream.close();
                    } catch (Exception ignored) {

                    }
                    inputStream = new FileInputStream(file);
                }
            } catch (Exception ignored) {

            }
        } else {
            try {
                inputStream.reset();
            } catch (Exception ignored) {

            }
        }
    }

    public void init() {
        try {
            streamReset();
            xmlSlideShow = new XMLSlideShow(inputStream);
            ppt = null;
            status = 1;
        } catch (Exception e) {
            //e.printStackTrace();
            //System.out.println();
            xmlSlideShow = null;
            try {
                streamReset();
                ppt = new HSLFSlideShow(inputStream);
                status = 2;
            } catch (Exception e2) {
                status = -1;
            }
        }
    }

    public int getStatus() {
        return status;
    }

    @Nullable
    private String getWorkName(List<String> lines) {
        String result = getWorkName2(lines);
        if (result != null) {
            if (result.toLowerCase().startsWith("тема:")) {
                result = result.substring("тема:".length()).trim();
            }
            result = result.trim();
        }
        return result;
    }

    @Nullable
    private String getWorkName2(List<String> lines) {
        try {
            int index_v1 = -1;
            for (int i = 0; i < lines.size(); i++) {
                if (lines.get(i).toLowerCase().startsWith("Выполнил".toLowerCase())) {
                    index_v1 = i - 1;
                    break;
                }
            }
            if (index_v1 == -1 || index_v1 >= lines.size()) {

            } else {
                for (int i = index_v1; i >= 0; i--) {
                    if (StringUtils.isNotBlank(lines.get(i))) {
                        String str = lines.get(i).trim();
                        if (str.startsWith("\"") || str.startsWith("«")) {
                            str = str.substring(1);
                        }
                        if (str.endsWith("\"") || str.endsWith("»")) {
                            str = str.substring(0,str.length()-1);
                        }
                        return str;
                    }
                }
            }
        } catch (Exception ignored) {

        }
        try {
            for (int i=0;i<lines.size();i++) {
                if (lines.get(i).toLowerCase().trim().startsWith("тема")) {
                    StringBuilder str = new StringBuilder(lines.get(i).trim().substring("тема".length()).trim());
                    if (str.toString().startsWith("\"") || str.toString().startsWith("«")) {
                        str = new StringBuilder(str.substring(1));
                        if (str.toString().endsWith("\"") || str.toString().endsWith("»")) {
                            str = new StringBuilder(str.substring(0, str.length() - 1));
                        } else {
                            i++;
                            while (i < lines.size()) {
                                String str2 = lines.get(i).trim();
                                if (str2.endsWith("\"") || str2.endsWith("»")) {
                                    break;
                                }
                                str2 = str2.substring(0,str2.length()-1);
                                str.append(" ").append(str2);
                                i++;
                            }
                        }
                    }
                    return str.toString();
                }
            }
        } catch (Exception ignored) {

        }
        return null;
    }

    @Nullable
    private String getStudentName(List<String> lines) {
        String result = null;
        try {
            String[] keys = new String[]{"Выполнила".toLowerCase(), "Выполнил".toLowerCase()};
            keysFor:
            for (String key : keys) {
                for (int j = 0; j < lines.size(); j++) {
                    String item = lines.get(j);
                    if (item.toLowerCase().trim().startsWith(key)) {
                        for (int i = key.length() + 1; i < item.length(); i++) {
                            if (Character.isLetter(item.charAt(i))) {
                                String item2 = item.substring(i);
                                String item3 = getNameFromString(item2);
                                if (item3 == null) {
                                    if (j + 1 < lines.size() && lines.get(j+1).split(" ").length <= 3) {
                                        item3 = getNameFromString(lines.get(j + 1));
                                    }
                                }
                                if (item3 != null) {
                                    result = item3.trim();
                                    break keysFor;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {

        }
        if (result == null) {
            try {
                String[] keys = new String[]{"Студент группы".toLowerCase()};
                keysFor:
                for (String key : keys) {
                    for (String item : lines) {
                        if (item.toLowerCase().trim().startsWith(key)) {
                            String[] arr = item.trim().substring(key.length()).trim().split(" ");
                            result = Arrays.stream(arr).skip(1).reduce((x1, x2) -> x1 + " " + x2).orElse(null);
                            break keysFor;
                        }
                    }
                }
            } catch (Exception ignored) {

            }
        }
        if (result != null) {
            if (result.contains(",")) {
                String[] arr = result.split(",");
                for (String word : arr) {
                    if (word.contains("@")) {
                        continue;
                    }
                    boolean if2 = true;
                    for (int i=0;i<word.length();i++) {
                        if (Character.isLetter(word.charAt(i))) {
                            continue;
                        }
                        if (word.charAt(i) == '.') {
                            continue;
                        }
                        if (Character.isSpaceChar(word.charAt(i))) {
                            continue;
                        }
                        if2 = false;
                    }
                    if (if2) {
                        result = word;
                        break;
                    }
                }
            }
        }
        return result;
    }

    private boolean isGroupNumber(String word) {
        word = word.trim();
        if (word.trim().isEmpty())
            return false;
        if (word.contains("@")) {
            return false;
        }
        if (!Character.isLetter(word.charAt(0))) {
            return false;
        }
        if (word.length() != "A18-502".length()) {
            return false;
        }
        boolean findTire = false;
        for (int i=1;i<word.length();i++) {
            if (Character.isLetterOrDigit(word.charAt(i))) {
                continue;
            }
            if (word.charAt(i) == '-') {
                findTire = true;
                continue;
            }
            return false;
        }
        if (findTire == true) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isWord(String word) {
        word = word.trim();
        if (word.isEmpty()) {
            return false;
        }
        for (char c : word.toCharArray()) {
            if (Character.isLetter(c) == false) {
                return false;
            }
        }
        return true;
    }

    private boolean isNameStartUpperCase(String word) {
        if (isWord(word) == false) {
            return false;
        }
        word = word.trim();
        if (Character.isUpperCase(word.charAt(0)) == false) {
            return false;
        }
        for (int i=1;i<word.length();i++) {
            if (Character.isLowerCase(word.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

    @Nullable
    @SuppressWarnings("Duplicates")
    private String getStudentGroup(List<String> lines) {
        try {
            String[] keys = new String[]{"Группа".toLowerCase()};
            for (String key : keys) {
                for (String item : lines) {
                    if (item.toLowerCase().trim().startsWith(key)) {
                        for (int i = key.length() + 1; i < item.length(); i++) {
                            if (Character.isLetter(item.charAt(i))) {
                                return item.substring(i).trim();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {

        }
        try {
            String[] keys = new String[]{"Студент группы".toLowerCase()};
            for (String key : keys) {
                for (String item : lines) {
                    if (item.toLowerCase().trim().startsWith(key)) {
                        String[] arr = item.trim().substring(key.length()).trim().split(" ");
                        for (String word : arr) {
                            if (isGroupNumber(word)) {
                                return word;
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {

        }
        try {
            String[] keys = new String[]{"Выполнила".toLowerCase(), "Выполнил".toLowerCase()};
            for (String key : keys) {
                for (String item : lines) {
                    if (item.toLowerCase().trim().startsWith(key)) {
                        String str = item.trim().replace(","," ").replace(".", " ").replace("  "," ");
                        String[] arr = str.split(" ");
                        for (String word : arr) {
                            if (isGroupNumber(word)) {
                                return word;
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {

        }
        return null;
    }

    @Nullable
    @SuppressWarnings("Duplicates")
    private String getTeacher(List<String> lines) {
        try {
            String[] keys = new String[] {"Преподаватель".toLowerCase(), "Руководитель".toLowerCase(), "проверила", "проверил"};
            for (String key : keys) {
                for (String item : lines) {
                    if (item.toLowerCase().trim().startsWith(key)) {
                        for (int i = key.length() + 1; i < item.length(); i++) {
                            if (Character.isLetter(item.charAt(i))) {
                                return item.substring(i).trim();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {

        }
        try {
            String[] keys = new String[] {"Преподаватель".toLowerCase(), "Руководитель".toLowerCase(), "проверила", "проверил"};
            for (String key : keys) {
                for (String item : lines) {
                    item = item.trim();
                    int index = item.indexOf(key);
                    if (index == -1) {
                        continue;
                    }
                    index += key.length();
                    while (index < item.length()) {
                        if (Character.isLetter(item.charAt(index))) {
                            break;
                        }
                        index++;
                    }
                    item = item.substring(index).trim();
                    String res = getNameFromString(item);
                    if (res != null) {
                        return res;
                    }
                }
            }
        } catch (Exception ignored) {

        }
        return null;
    }

    private String getNameFromString(String item) {
        List<String> list = new ArrayList<>(Arrays.asList(item.split(" ")));
        list.removeIf(this::isGroupNumber);
        if (list.size() < 2) {
            return null;
        }
        String surname = list.get(0);
        if (isNameStartUpperCase(surname) == false) {
            return null;
        }
        if (list.get(1).endsWith(".")) {
            if (list.get(1).length() == 2) {
                surname += " " + list.get(1);
                if (list.size() >= 3 && list.get(2).endsWith(".")) {
                    surname += " " + list.get(2);
                }
            } else if (list.get(1).length() == 4) {
                String tmp2 = list.get(1);
                String[] arr = tmp2.split("\\.");
                List<String> nameW = Arrays.stream(arr)
                        //.filter(x-> x.length() == 1)
                        //.filter(x-> Character.isLetter(x.charAt(0)))
                        .collect(Collectors.toList());
                if (nameW.size() <= 2) {
                    for (String item2 : nameW) {
                        surname += " " + item2 + ".";
                    }
                }
            }
        } else {
            if (isNameStartUpperCase(list.get(1))) {
                surname += " " + list.get(1);
            }
            if (isNameStartUpperCase(list.get(2))) {
                surname += " " + list.get(2);
            }
        }
        return surname;
    }

    @Nullable
    private String getEmail(List<String> lines) {
        try {
            for (String item : lines) {
                if (item.toLowerCase().trim().startsWith("email")) {
                    String[] arr = item.split(" ");
                    return arr[arr.length - 1];
                }
                if (item.toLowerCase().trim().startsWith("e-mail")) {
                    String[] arr = item.split(" ");
                    return arr[arr.length - 1];
                }
            }
        } catch (Exception ignored) {

        }
        try {
            for (String item : lines) {
                String[] words = item.split(" ");
                for (String word : words) {
                    int dog = word.indexOf("@");
                    int dot = word.lastIndexOf(".");
                    if (dog < 1)
                        continue;
                    if (dot >= word.length())
                        continue;
                    if (dog >= dot)
                        continue;
                    return word;
                }
            }
        } catch (Exception ignored) {

        }
        return null;
    }

    public Presentation_V1 getV1() {
        Presentation_V1 presentation_v1 = new Presentation_V1();
        if (xmlSlideShow != null) {
            presentation_v1.setSlideCount(xmlSlideShow.getSlides().size());
        } else if (ppt != null) {
            presentation_v1.setSlideCount(ppt.getSlides().size());
        }
        List<String> lines = new ArrayList<>();
        if (xmlSlideShow != null) {
            XSLFSlide slide = xmlSlideShow.getSlides().get(0);
            slide.getShapes().stream().map(x-> {
                try {
                    return x.getClass().getMethod("getText").invoke(x);
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    e.printStackTrace();
                }
                return null;
            }).filter(Objects::nonNull).map(Object::toString).map(x-> x.split("\n")).map(Arrays::asList).forEachOrdered(lines::addAll);
//            Arrays.stream(slide.getPlaceholders()).map(XSLFTextShape::getText).map(x -> x.split("\n"))
//                    .map(Arrays::asList).forEachOrdered(lines::addAll);
            if (lines.size() == 0) {
                //System.out.println(slide.getPlaceholders().length);
                //System.out.println(slide.getTitle());
                slide.getShapes().stream().map(x-> {
                    try {
                        XSLFTextBox item = (XSLFTextBox) x;
                        return item.getText();
                    } catch (Exception ignored) {
                        try {
                            XSLFTextShape item = (XSLFTextShape) x;
                            return item.getText();
                        } catch (Exception ignored2) {

                        }
                    }
                    return null;
                }).filter(StringUtils::isNotBlank).map(x-> x.split("\n")).map(Arrays::asList).forEachOrdered(lines::addAll);
            }
        } else if (ppt != null) {
            HSLFSlide slide = ppt.getSlides().get(0);
            slide.getShapes().stream().map(x-> {
                try {
                    return x.getClass().getMethod("getText").invoke(x);
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    e.printStackTrace();
                }
                return null;
            }).filter(Objects::nonNull).map(Object::toString).filter(StringUtils::isNotBlank).map(x-> x.split("\n")).map(Arrays::asList)
                    .forEachOrdered(lines::addAll);
        }

        String workName = getWorkName(lines);
        String studentName = getStudentName(lines);
        String group = getStudentGroup(lines);
        String teacher = getTeacher(lines);
        String email = getEmail(lines);

        presentation_v1.setTitle(workName);
        presentation_v1.setStudentName(studentName);
        presentation_v1.setGroup(group);
        presentation_v1.setEmail(email);

        if (StringUtils.isNotBlank(fileName)) {
            presentation_v1.setFilename(fileName);
        }

        return presentation_v1;
    }

    public static void main(String[] args) throws IOException {
        //2019_met_M17_115_GolosovOV_positivism.pptx
        //2019_met_M17_110_KamitovaVV_synergetica.pptx
        String path = "/Users/alekseyfrolov/Downloads/111/phi7/2019-Met_M17-163_Ахмед-Мохамед_Phenomenology(1).ppt";
        //String path = "/Users/alekseyfrolov/Downloads/Presentation1.ppt";
        File file = new File(path);
        FileInputStream fileInputStream = new FileInputStream(file);
        PresentationParser presentationParser = new PresentationParser(fileInputStream);
        presentationParser.setFileName(path);
        presentationParser.init();
        Presentation_V1 presentation_v1 = presentationParser.getV1();
    }
}
