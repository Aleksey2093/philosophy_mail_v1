package com.mephi.Parsers;

import com.mephi.Data.Presentation_V1;

public interface PresentationInterface {

    Presentation_V1 getV1();
}
