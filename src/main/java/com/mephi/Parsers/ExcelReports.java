package com.mephi.Parsers;

import com.mephi.Data.Presentation_V1;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;

public class ExcelReports {

    private HSSFWorkbook workbook;
    private HSSFSheet sheet;
    private int rowNum = 0;

    public ExcelReports () {
        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet("sheet");
        Cell cell;
        Row row;

        row = sheet.createRow(rowNum);
        //ФИО, адрес, группа, название презентации, количество слайдов  в ней, либо, хотя бы, мегабайтаж
        cell = row.createCell(0, CellType.STRING);
        cell.setCellValue("ФИО");

        cell = row.createCell(1,CellType.STRING);
        cell.setCellValue("Адрес");

        cell = row.createCell(2,CellType.STRING);
        cell.setCellValue("Группа");

        cell = row.createCell(3,CellType.STRING);
        cell.setCellValue("Название презентации");

        cell = row.createCell(4, CellType.STRING);
        cell.setCellValue("Кол-во слайдов");

        cell = row.createCell(5,CellType.STRING);
        cell.setCellValue("Наименование файла");

        rowNum++;
    }

    public void add(Presentation_V1 presentation_v1) {
        Row row = sheet.createRow(rowNum);
        Cell cell = row.createCell(0, CellType.STRING);
        cell.setCellValue(presentation_v1.getStudentName());

        cell = row.createCell(1,CellType.STRING);
        cell.setCellValue(presentation_v1.getEmail());

        cell = row.createCell(2,CellType.STRING);
        cell.setCellValue(presentation_v1.getGroup());

        cell = row.createCell(3,CellType.STRING);
        cell.setCellValue(presentation_v1.getTitle());

        cell = row.createCell(4, CellType.STRING);
        cell.setCellValue(presentation_v1.getSlideCount());

        cell = row.createCell(5,CellType.STRING);
        cell.setCellValue(presentation_v1.getFilename());

        for (int i=0;i<6;i++) {
            sheet.autoSizeColumn(i);
        }

        rowNum++;
    }

    public void addAll(Collection<Presentation_V1> presentationV1Collection) {
        presentationV1Collection.forEach(this::add);
    }

    public void save(FileOutputStream fileOutputStream) throws IOException {
        workbook.write(fileOutputStream);
    }
}
