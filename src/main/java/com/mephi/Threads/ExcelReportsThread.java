package com.mephi.Threads;

import com.mephi.Data.Presentation_V1;
import com.mephi.Data.Value;
import com.mephi.GUI.MainWindow;
import com.mephi.Parsers.ExcelReports;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ExcelReportsThread implements Runnable {
    private int count = 1;
    private String path;
    private LinkedBlockingQueue<Presentation_V1> presentations;
    private Component parentComponent;

    public ExcelReportsThread(String path, LinkedBlockingQueue<Presentation_V1> presentations, Component parentComponent) {
        this.path = path;
        this.presentations = presentations;
        this.parentComponent = parentComponent;
    }

    private String getNewExcelPath(String folder) {
        if (folder.endsWith("/") == false)
            folder = folder + "/";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm");
        String path = folder + simpleDateFormat.format(new Date()) + ".xls";
        int i = 2;
        while (new File(path).exists()) {
            path = folder + simpleDateFormat.format(new Date()) + " (" + i + ").xls";
            i++;
        }
        return path;
    }

    private ExcelReports saveExcel(ExcelReports excelReports, String path, Presentation_V1 presentation) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(path);
            if (excelReports == null) {
                excelReports = new ExcelReports();
            }
            System.out.println(count + ": " + presentation.getFilename() + " презентация этап 5");
            excelReports.add(presentation);
            excelReports.save(fileOutputStream);
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(count + ": " + presentation.getFilename() + " презентация этап 6");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(parentComponent, "Ошибка при сохранении файла excel (1)\n" + e.toString(), "Ошибка", JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(parentComponent, "Ошибка при сохранении файла excel (2)\n" + e.toString(), "Ошибка", JOptionPane.ERROR_MESSAGE);
        }
        count++;
        return excelReports;
    }

    @Override
    public void run() {
        ExcelReports excelReports = new ExcelReports();
        String excelPath = getNewExcelPath(path);
        do {
            try {
                Presentation_V1 presentation_v1 = presentations.poll(3, TimeUnit.SECONDS);
                if (presentation_v1 != null) {
                    saveExcel(excelReports, excelPath, presentation_v1);
                }
            } catch (InterruptedException e) {
                break;
            }
        } while (!Thread.interrupted());
        while (presentations.size() > 0) {
            Presentation_V1 presentation_v1 = presentations.poll();
            if (presentation_v1 != null) {
                saveExcel(excelReports, excelPath, presentation_v1);
            }
        }
        try {
            Presentation_V1 presentation_v1 = presentations.poll(1, TimeUnit.SECONDS);
            if (presentation_v1 != null) {
                saveExcel(excelReports, excelPath, presentation_v1);
            }
        } catch (InterruptedException ignored) {

        }
    }
}
