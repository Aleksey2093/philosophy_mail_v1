package com.mephi.Threads;

import com.mephi.Data.AttachmentData;
import com.mephi.Data.Value;
import com.mephi.Parsers.PresentationParser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class AttachmentQueueThread implements Runnable {

    private final Value<Integer> count;
    private LinkedBlockingQueue<AttachmentData> queue;
    private LinkedBlockingQueue<PresentationParser> presentationParsers;

    public AttachmentQueueThread(Value<Integer> count, LinkedBlockingQueue<AttachmentData> queue, LinkedBlockingQueue<PresentationParser> presentationParsers) {
        this.count = count;
        this.queue = queue;
        this.presentationParsers = presentationParsers;
    }

    @Override
    public void run() {
        try {
            do {
                work(3);
            } while (!Thread.interrupted());
            while (queue.size() > 0) {
                work(0);
            }
            work(0);
        } catch (InterruptedException e) {

        }
    }

    private void work(int timeout) throws InterruptedException {
        AttachmentData attachmentData;
        if (timeout > 0) {
            attachmentData = queue.poll(timeout, TimeUnit.SECONDS);
        } else {
            attachmentData = queue.poll();
        }
        if (attachmentData != null) {
            work2(attachmentData);
        }
    }

    private void work2(AttachmentData attachmentData) {
        int count;
        synchronized (this.count) {
            count = this.count.getValue();
        }
        System.out.println(count + ": " + " сохранение файла " + attachmentData.getFile().getName());
        try {
            if (attachmentData.getFile().exists() && attachmentData.getInputStream() == null) {
                System.out.println(count + ": " + " не удалось получить файл " + attachmentData.getFile().getName());
                attachmentData.getFile().delete();
            } else {
                FileOutputStream out = new FileOutputStream(attachmentData.getFile(), false);
                org.apache.commons.io.IOUtils.copyLarge(attachmentData.getInputStream(), out);
                out.close();
                FileInputStream fileInputStream = new FileInputStream(attachmentData.getFile());
                PresentationParser presentationParser = new PresentationParser(fileInputStream);
                presentationParser.setFileName(attachmentData.getPath());
                if (presentationParsers != null) {
                    presentationParsers.add(presentationParser);
                }
            }
            try {
                attachmentData.getInputStream().close();
            } catch (Exception ignored) {

            }
            System.out.println(count + ": " + " завершено сохранение файла " + attachmentData.getFile().getName());
        } catch (Exception e) {
            System.out.println(count + ": " + " ошибка при сохранении файла " + attachmentData.getFile().getName());
            e.printStackTrace();
            PresentationParser presentationParser = new PresentationParser(attachmentData.getInputStream());
            presentationParser.setFileName(attachmentData.getPath());
            if (presentationParsers != null) {
                presentationParsers.add(presentationParser);
            }
        }
        synchronized (this.count) {
            this.count.setValue(this.count.getValue() + 1);
        }
    }
}
