package com.mephi.Threads;

import com.mephi.Data.MessageData;
import com.mephi.Data.Value;
import netscape.javascript.JSObject;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class MailResponseThread implements Runnable {

    private Session session;
    private String from;
    private LinkedBlockingQueue<MessageData> queue;
    private String responseText;

    public MailResponseThread(Session session, String from, String responseText, LinkedBlockingQueue<MessageData> queue) {
        this.session = session;
        this.from = from;
        this.queue = queue;
        this.responseText = responseText;
    }

    private void iteration(int timeout) throws InterruptedException {
        try {
            MessageData data;
            if (timeout > 0) {
                data = queue.poll(timeout, TimeUnit.SECONDS);
            } else {
                data = queue.poll();
            }
            if (data == null)
                return;
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(data.getTo()));
            message.setSubject("RE: " + data.getTitle());
            if (StringUtils.isNotBlank(data.getText())) {
                message.setText(responseText + "\n\n\nЦитита:\n" + data.getText());
            } else {
                message.setText(responseText);
            }
            //Transport transport = session.getTransport("smtp");
            //transport.connect();
            //transport.sendMessage(message,message.getAllRecipients());
            Transport.send(message);
            System.out.println("Send");
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            do {
                iteration(3);
            } while (!Thread.currentThread().isInterrupted());
        } catch (InterruptedException e) {

        }
        while (queue.size() > 0) {
            try {
                iteration(0);
            } catch (InterruptedException e) {

            }
        }
        try {
            iteration(0);
        } catch (InterruptedException e) {

        }
    }
}
