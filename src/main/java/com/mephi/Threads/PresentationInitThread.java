package com.mephi.Threads;

import com.mephi.Data.Presentation_V1;
import com.mephi.Data.Value;
import com.mephi.Parsers.PresentationParser;
import org.omg.PortableServer.THREAD_POLICY_ID;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class PresentationInitThread implements Runnable {
    private final Value<Integer> count;
    private LinkedBlockingQueue<PresentationParser> presentations;
    private LinkedBlockingQueue<Presentation_V1> presentationsForSave;

    public PresentationInitThread(Value<Integer> count, LinkedBlockingQueue<PresentationParser> presentations, LinkedBlockingQueue<Presentation_V1> presentationsForSave) {
        this.count = count;
        this.presentations = presentations;
        this.presentationsForSave = presentationsForSave;
    }


    @Override
    public void run() {
        try {
            do {
                work(3);
            } while (!Thread.interrupted());
        } catch (InterruptedException e) {

        }
        while (presentations.size() > 0) {
            try {
                work(0);
            } catch (InterruptedException e) {

            }
        }
        try {
            work(0);
        } catch (InterruptedException e) {

        }
    }

    private void work(int timeout) throws InterruptedException {
        PresentationParser parser;
        if (timeout > 0) {
            parser = presentations.poll(timeout, TimeUnit.SECONDS);
        } else {
            parser = presentations.poll();
        }
        if (parser != null) {
            work2(parser);
        }
    }

    private void work2(final PresentationParser parser) {
        int count;
        synchronized (this.count) {
            count = this.count.getValue();
        }
        try {
            System.out.println(count + ": " + parser.getFileName() + " презентация этап 1");
            Thread thread = new Thread(parser::init);
            thread.start();
            try {
                thread.join(1000 * 60);
            } catch (InterruptedException ignored) {
                Thread.currentThread().interrupt();
            }
            System.out.println(count + ": " + parser.getFileName() + " презентация этап 2");
            if (parser.getStatus() == 1 || parser.getStatus() == 2) {
                Presentation_V1 presentation_v1 = parser.getV1();
                if (presentation_v1 != null) {
                    presentationsForSave.add(presentation_v1);
                    System.out.println(count + ": " + parser.getFileName() + " презентация этап 3");
                } else {
                    System.out.println(count + ": " + parser.getFileName() + " презентация этап 3 ошибка");
                }
            } else {
                System.out.println(count + ": " + parser.getFileName() + " презентация этап 3 ошибка (" + parser.getStatus() + ")");
            }
            try {
                parser.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(count + ": " + parser.getFileName() + " презентация этап 4");
        } catch (Exception e) {
            e.printStackTrace();
        }
        synchronized (this.count) {
            this.count.setValue(this.count.getValue() + 1);
        }
    }
}
